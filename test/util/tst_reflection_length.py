from __future__ import division, print_function

from xfel.util.reflection_length import ReflectionsRadialLengths, ReflectionsRadialLengthsFromFiles
from dials.util.options import Importer, flatten_reflections, flatten_experiments
import libtbx.load_env
import shutil, os
from libtbx.test_utils import open_tmp_directory
from libtbx.test_utils import approx_equal

class test_reflection_length(object):
  def __init__(self):
    self.xfel_regression_dir = libtbx.env.find_in_repositories(
      relative_path="xfel_regression",
      test=os.path.isdir)
    self.test_multipanel_strong_pickle = os.path.join(self.xfel_regression_dir, "processing_test_data",
      "multipanel_dials_intermediates", "idx-cxi_data_00005_strong.pickle")
    self.test_multipanel_experiments = os.path.join(self.xfel_regression_dir, "processing_test_data",
      "multipanel_dials_intermediates", "idx-cxi_data_00005_refined_experiments.json")
    self.test_single_panel_strong_pickle = os.path.join(self.xfel_regression_dir, "processing_test_data",
      "single_panel_dials_intermediates", "idx-mfx_data_00005_strong.pickle")
    self.test_single_panel_experiments = os.path.join(self.xfel_regression_dir, "processing_test_data",
      "single_panel_dials_intermediates", "idx-mfx_data_00005_refined_experiments.json")

    self.cwd = os.path.abspath(os.curdir)
    self.tmp_dir = open_tmp_directory(suffix="test_reflection_length")
    os.chdir(self.tmp_dir)
    for file in [self.test_multipanel_experiments, self.test_multipanel_strong_pickle,
      self.test_single_panel_experiments, self.test_single_panel_strong_pickle]:
      assert os.path.exists(file)
      shutil.copy(file, self.tmp_dir)
    print ("reflection_length test files located at %s" % self.tmp_dir)

  def get_spot_lengths_px(self):
    multipanel_importer = Importer([
      self.test_multipanel_strong_pickle,
      self.test_multipanel_experiments],
      read_experiments=True,
      read_reflections=True, check_format=False)
    assert not multipanel_importer.unhandled
    self.multipanel_strong = flatten_reflections(multipanel_importer.reflections)
    assert len(self.multipanel_strong) == 1
    self.multipanel_expt = flatten_experiments(multipanel_importer.experiments)
    assert len(self.multipanel_expt) == 1

    single_panel_importer = Importer([
      self.test_single_panel_strong_pickle,
      self.test_single_panel_experiments],
      read_experiments=True,
      read_reflections=True, check_format=False)
    assert not single_panel_importer.unhandled
    self.single_panel_strong = flatten_reflections(single_panel_importer.reflections)
    assert len(self.single_panel_strong) == 1
    self.single_panel_expt = flatten_experiments(single_panel_importer.experiments)
    assert len(self.single_panel_expt) == 1

    multipanel_strong_spot_lengths = ReflectionsRadialLengths(
      self.multipanel_strong[0], experiment=self.multipanel_expt[0]).get_spot_lengths_px()
    assert len(multipanel_strong_spot_lengths) == 37
    assert approx_equal(multipanel_strong_spot_lengths[10], 2.4710250319461977), multipanel_strong_spot_lengths[10]

    single_panel_strong_spot_lengths = ReflectionsRadialLengths(
      self.single_panel_strong[0], experiment=self.single_panel_expt[0]).get_spot_lengths_px()
    assert len(single_panel_strong_spot_lengths) == 297
    assert approx_equal(single_panel_strong_spot_lengths[10], 2.0462368420796793), single_panel_strong_spot_lengths[10]

    print ("OK")

  def get_spot_lengths_px_from_files(self):
    multipanel_strong_spot_lengths = ReflectionsRadialLengthsFromFiles([
      self.test_multipanel_strong_pickle, self.test_multipanel_experiments]).get_spot_lengths_px()
    assert len(multipanel_strong_spot_lengths) == 37
    assert approx_equal(multipanel_strong_spot_lengths[10], 2.4710250319461977), multipanel_strong_spot_lengths[10]

    single_panel_strong_spot_lengths = ReflectionsRadialLengthsFromFiles([
      self.test_single_panel_strong_pickle, self.test_single_panel_experiments]).get_spot_lengths_px()
    assert len(single_panel_strong_spot_lengths) == 297
    assert approx_equal(single_panel_strong_spot_lengths[10], 2.0462368420796793), single_panel_strong_spot_lengths[10]

    print ("OK")

  def run_all(self):
    self.get_spot_lengths_px()
    self.get_spot_lengths_px_from_files()
    os.chdir(self.cwd)

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  tester = test_reflection_length()
  tester.run_all()
