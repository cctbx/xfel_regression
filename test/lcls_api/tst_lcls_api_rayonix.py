from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
from scitbx import matrix
import os, shutil

message = ''' Test script for testing the lcls/cctbx API on **Rayonix** data obtained at LCLS
              Note that the script runs the API and compares the following with the provided
              reference results
              1. Beam model
              2. Crystal models
              3. Detector models
              4. Number of reflections in the integrated reflection tables'''


class test_lcls_cctbx_api(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

    self.xfel_dir = libtbx.env.find_in_repositories(
        relative_path="xfel",
        test=os.path.isdir)


  def test_rayonix(self):
    if self.xfel_regression is None:
      print ("Skipping Rayonix lcls/cctbx API regression test: xfel_regression not present")
      return

    mfx_dir = os.path.join(self.xfel_regression, "image_examples/LCLS_XTC_data")
    if not os.path.exists(mfx_dir):
      print ("Skipping Rayonix lcls/cctbx API regression test: LCLS_XTC_data not present")
      return

    # Set necessary environmental variables for PSANA to work
    os.environ['SIT_DATA']=os.path.join(mfx_dir,"regg/psdm/data")
    os.environ['SIT_PSDM_DATA']=mfx_dir

    # Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_mfx0")
    os.chdir(tmp_dir)


    print ("Assertion results will be found at", tmp_dir)

    # Set up command for API test
    phil_file_name = 'params_rayonix.phil'
    phil_file = os.path.join(self.xfel_regression, 'test', 'lcls_api', phil_file_name)
    mask_file = os.path.join(self.xfel_regression, 'test', 'xtc_process', 'test_xtc_process_mfx', 'input', 'pixels.mask')
    shutil.copy(phil_file, phil_file_name)
    with open(phil_file_name, 'a') as phil_fh:
      phil_fh.write('spotfinder.lookup.mask=%s\nintegration.lookup.mask=%s'%(mask_file, mask_file))
    script_path = os.path.join(self.xfel_dir, 'lcls_api/exercise_api.py')
    command = os.path.join("cctbx.python %s mfxo1916 20 Rayonix %s 0"%(script_path, phil_file_name))
    print (command)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()
    with open('out.log', 'w') as fout:
      result.show_stdout(out=fout)

    # Now compare results vs reference results provided
    deviation = 7 # Difference in number of reflections compared to reference results
                  # Taken from test_stills_process. Will depend on machine arch
    # First load in the reference models. Comparing only integrated expt/refls
    from dxtbx.model.experiment_list import ExperimentListFactory
    from dials.array_family import flex
    # load reference experiments
    ref_dir = os.path.join(self.xfel_regression, 'test', 'lcls_api', 'reference_results')
    ref_exp = ExperimentListFactory.from_json_file(os.path.join(ref_dir, 'idx-mfxo1916_run20_0_integrated.expt'), check_format=False)
    ref_refls = flex.reflection_table.from_file(os.path.join(ref_dir, 'idx-mfxo1916_run20_0_integrated.refl'))
    # Now load the experiments and corresponding refls from the current processing
    curr_exp = ExperimentListFactory.from_json_file('idx-mfxo1916_run20_0_integrated.expt', check_format=False)
    curr_refls = flex.reflection_table.from_file('idx-mfxo1916_run20_0_integrated.refl')

    # Now compare models for each frame
    # First beam model
    tol = 1.e-5
    b1 = curr_exp.beams()
    b2 = ref_exp.beams()
    assert len(b1) == len(b2) == 1, 'Should have only one beam model'
    assert b1[0].is_similar_to(b2[0], wavelength_tolerance=tol,
                                direction_tolerance=tol,
                                polarization_normal_tolerance=tol,
                                polarization_fraction_tolerance=tol), 'Beam models dissimilar. Please check output directory'
    s0_1 = matrix.col(b1[0].get_unit_s0())
    s0_2 = matrix.col(b2[0].get_unit_s0())
    assert s0_1.accute_angle(s0_2, deg=True) < 0.0057, 's0 vector dissimilar. Please check output directory' # ~0.1 mrad
    # Now test crystal models
    c1 = curr_exp.crystals()
    c2 = ref_exp.crystals()
    assert len(c1) == len(c2) == 1, 'Should have only one crystal model'
    assert c1[0].is_similar_to(c2[0]) ,'Crystal models dissimilar. Please check output directory'
    # Now test detector models
    d1 = curr_exp.detectors()
    d2 = ref_exp.detectors()
    assert len(d1) == len(d2) == 1, 'Should have only one detector model'
    assert d1[0].is_similar_to(d2[0],
      fast_axis_tolerance=1e-4, slow_axis_tolerance=1e-4, origin_tolerance=1e-2), 'Detector models dissimilar. Please check output directory'

    nrefls_curr = len(curr_refls)
    nrefls_ref = len(ref_refls)
    assert nrefls_curr in range(nrefls_ref-deviation, nrefls_ref+deviation), 'Mismatch in integrated reflections too high. Please check output files'

    print ("OK")

  def run_all(self):
    self.test_rayonix()

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  print (message)
  show_times_at_exit()
  tester = test_lcls_cctbx_api()
  tester.run_all()
