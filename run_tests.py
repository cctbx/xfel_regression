from __future__ import division
from libtbx import test_utils
import libtbx.load_env
import platform
# from libtbx.test_utils.pytest import discover

tst_list = [
            "$D/test/command_line/tst_cspad_quadrants.py",
            "$D/test/util/tst_reflection_length.py",
            "$D/test/util/tst_jungfrau.py",
            ["$D/test/command_line/tst_cxi_merge.py", "1"],
            ["$D/test/command_line/tst_cxi_merge.py", "2"],
            ["$D/test/command_line/tst_cxi_merge.py", "7"],
            ["$D/test/command_line/tst_cspad_cbf_metrology.py", "300"],
            ["$D/test/command_line/tst_xfel_merge.py", "all_1"],
            ["$D/test/command_line/tst_xfel_merge.py", "all_2"],
            ["$D/test/command_line/tst_xfel_merge.py", "input"],
            ["$D/test/command_line/tst_xfel_merge.py", "model_scaling"],
            ["$D/test/command_line/tst_xfel_merge.py", "modify"],
            ["$D/test/command_line/tst_xfel_merge.py", "filter"],
            ["$D/test/command_line/tst_xfel_merge.py", "modify_reindex_to_reference"],
            ["$D/test/command_line/tst_xfel_merge.py", "errors_premerge"],
            ["$D/test/command_line/tst_xfel_merge.py", "scale"],
            ["$D/test/command_line/tst_xfel_merge.py", "mark1"],
            ["$D/test/command_line/tst_xfel_merge.py", "postrefine"],
            ["$D/test/command_line/tst_xfel_merge.py", "statistics_unitcell"],
            ["$D/test/command_line/tst_xfel_merge.py", "statistics_beam"],
            ["$D/test/command_line/tst_xfel_merge.py", "model_statistics"],
            ["$D/test/command_line/tst_xfel_merge.py", "statistics_resolution"],
            ["$D/test/command_line/tst_xfel_merge.py", "group"],
            ["$D/test/command_line/tst_xfel_merge.py", "errors_merge"],
            ["$D/test/command_line/tst_xfel_merge.py", "errors_merge_ev11"],
            ["$D/test/command_line/tst_xfel_merge.py", "errors_merge_mm24"],
            #["$D/test/command_line/tst_xfel_merge.py", "statistics_deltaccint"],
            ["$D/test/command_line/tst_xfel_merge.py", "statistics_intensity"],
            ["$D/test/command_line/tst_xfel_merge.py", "merge"],
            ["$D/test/command_line/tst_xfel_merge.py", "filter_global"],
            "$D/test/util/tst_TargetWithFastRij.py",
            "$D/test/command_line/tst_small_cell_process.py",
            "$D/test/command_line/tst_powder_from_spots.py",
            "$D/merging_test_data/cosym_input_data/test_cosym_tranch_align.py",
            #["$D/test/command_line/tst_xfel_merge.py", "statistics_intensity_cxi"],
            ]

tst_list_slow = [
            ["$D/test/command_line/tst_cxi_merge.py", "3"],
            ["$D/test/command_line/tst_cxi_merge.py", "4"],
            ["$D/test/command_line/tst_cxi_merge.py", "5"],
            ["$D/test/command_line/tst_cxi_merge.py", "6"],
            "$D/test/prime/tst_end2end.py",
            "$D/test/prime/tst_index_ambiguity_end2end.py",
            #["$D/test/command_line/tst_cspad_cbf_metrology.py", "3000"], # too long
            ]

tst_list_parallel = [
            ]

try:
  import labelit # import dependency
except ImportError:
  pass
else:
  tst_list.append("$D/test/command_line/tst_cxi_index.py")


try:
    import psana # import dependency
except ImportError:
    pass
except TypeError:
    # Check if SIT_* environment variables are set
    import os

    if os.environ.get("SIT_ROOT"):
        # Variables are present, so must have been another error
        raise
    pass
else:
  tst_list.append("$D/test/command_line/tst_mfx_formatXTC.py")
  # Temporarily disabled 09/15/22 due to https://github.com/cctbx/dxtbx/pull/536
  #tst_list.append("$D/test/command_line/tst_cxi_formatXTC.py")
  tst_list.append("$D/test/xtc_process/test_xtc_process_mfx/tst_xtc_process_mfx.py")
  tst_list.append("$D/test/xtc_process/test_xtc_process_cxi/tst_xtc_process_cxi.py")
  tst_list.append("$D/test/lcls_api/tst_lcls_api_rayonix.py")

if platform.platform().startswith("Linux"):
  # modify_cosym_p1 is to test the case of four-fold ambiguity within cctbx.xfel's
  # version of cosym. This is still a topic of research
  mpi_tests = [
      ["$D/test/command_line/tst_xfel_merge.py", "modify_cosym"],
      #["$D/test/command_line/tst_xfel_merge.py", "modify_cosym_p1"],
      ["$D/test/command_line/tst_ensemble_refinement.py"],
      ["$D/test/util/tst_mpi_abort.py"],
  ]
else:
  mpi_tests = []

try:
  import mpi4py
except ImportError:
  print("mpi4py not found. Skipping tests:")
  print(mpi_tests)
else:
  tst_list_parallel.extend(mpi_tests)

print("Attempting to find lunus")
try:
  import lunus.command_line # import dependency
except ImportError:
  print("Lunus not found")
else:
  print("Lunus available, running pytests")
  # tmp_tests = discover(module='xfel_regression')
  # if tmp_tests:
  #   tst_list_parallel.extend(tmp_tests)
  #   tst_list_parallel[-1].append('--regression')
  # else:
  #   assert False, "pytest couldn't find the lunus tests"

def run():
  build_dir = libtbx.env.under_build("xfel_regression")
  dist_dir = libtbx.env.dist_path("xfel_regression")
  test_utils.run_tests(build_dir, dist_dir, tst_list)

if (__name__ == "__main__"):
  import sys
  if 'slow_tests=True' in sys.argv:
    tst_list.extend(tst_list_slow)
  run()

# libtbx.run_tests_parallel module=xfel_regression
