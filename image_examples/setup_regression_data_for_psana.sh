#!/bin/bash

# This script puts the LCLS XTC example data in your path

export SIT_DATA=`libtbx.find_in_repositories xfel_regression`/image_examples/LCLS_XTC_data/regg/psdm/data
export SIT_ROOT=`libtbx.find_in_repositories xfel_regression`/image_examples/LCLS_XTC_data
export SIT_PSDM_DATA=$SIT_ROOT
