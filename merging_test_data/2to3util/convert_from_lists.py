from __future__ import division, print_function
from cctbx import sgtbx # import dependency
from libtbx import easy_pickle
from cctbx.crystal_orientation import crystal_orientation
from cctbx import crystal, miller
from cctbx.array_family import flex
import sys, os

"""
Script to read cxi.merge pickle converted to lists and convert back to flex arrays
Used to convert pickles from py2 to py3
"""

filepath = sys.argv[1]
d = easy_pickle.load(filepath)

new_d = {}
for key in d:
  print (key)
  if key == 'current_orientation':
    new_d['current_orientation'] = [crystal_orientation(d['current_orientation'][0])]
  elif key == 'mapped_predictions':
    a, b = d['mapped_predictions']
    new_d['mapped_predictions'] = [flex.vec2_double(flex.double(a), flex.double(b))]
  elif key == 'correction_vectors':
    new_d[key] = d[key]
  elif key == 'observations':
    uc, info, indices, data, sigmas = d['observations']
    sym = crystal.symmetry(uc, str(info))
    miller_set = miller.set(sym, flex.miller_index(indices))
    obs = miller.array(miller_set, flex.double(data), flex.double(sigmas)).set_observation_type_xray_intensity()
    new_d[key] = [obs]
  elif isinstance(d[key], list):
    assert len(d[key]) == 1
    if hasattr(d[key][0], '__len__') and not isinstance(d[key][0],str):
      if isinstance(d[key][0][0], tuple):
        new_d[key] = [flex.miller_index(d[key][0])]
      elif isinstance(d[key][0][0], float):
        new_d[key] = [flex.double(d[key][0])]
      elif isinstance(d[key][0][0], int):
        new_d[key] = [flex.int(d[key][0])]
      else:
        assert False
    else:
      new_d[key] = d[key]
  elif hasattr(d[key], '__len__') and not isinstance(d[key][0],str):
    if isinstance(d[key][0], int):
      new_d[key] = [flex.int(d[key])]
    else:
      assert False
  else:
    new_d[key] = d[key]

easy_pickle.dump(os.path.basename(filepath), new_d)
