from __future__ import division, print_function
import libtbx.load_env
import os, sys
from libtbx import easy_run
from dxtbx.model.experiment_list import ExperimentListFactory
from libtbx.test_utils import open_tmp_directory
from scitbx import matrix

class test_cspad_cbf_metrology(object):
  def __init__(self):

    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def test_refine_metrology(self, dataset, method="expanding"):
    if self.xfel_regression is None:
      print ("Skipping test_refine_metrology: xfel_regression not present")
      return

    assert dataset in [300, 3000]
    assert method in ["hierarchical", "expanding"]

    data_dir = os.path.join(self.xfel_regression, 'cspad_cbf_metrology')

    cwd = os.getcwd()
    tmp_dir = open_tmp_directory(suffix="test_refine_metrology_%d"%dataset)
    os.chdir(tmp_dir)
    f = open("combine.phil", 'w')
    f.write("""
    input {
      experiments = %s%sinput_combined_experiments_%d.json
      reflections = %s%sinput_combined_reflections_%d.pickle
    }
    """%(data_dir, os.path.sep, dataset,
         data_dir, os.path.sep, dataset))
    f.close()

    command = "cspad.cbf_metrology flat_refinement=True flat_refinement_with_distance=True method=%s data_phil=combine.phil"%method
    print (command)
    result = easy_run.fully_buffered(command=command).raise_if_errors()

    result.show_stdout()
    result.show_stderr()

    # load results
    if method == "hierarchical":
      reg_exp = ExperimentListFactory.from_json_file(
                  os.path.join(data_dir, "regression_%d_hierarchical_final.json"%dataset),
                  check_format=False)
      ref_exp = "cspad_refined_level2.expt"
    else:
      reg_exp = ExperimentListFactory.from_json_file(
                  os.path.join(data_dir, "regression_%d_final.json"%dataset),
                  check_format=False)
      ref_exp = "cspad_refined_step7_level2.expt"
      if not os.path.isfile(ref_exp):
        ref_exp = "cspad_refined_experiments_step7_level2.json"
    ref_exp = ExperimentListFactory.from_json_file(ref_exp, check_format=False)

    # compare results
    tol = 1e-5
    for b1, b2 in zip(reg_exp.beams(), ref_exp.beams()):
      assert b1.is_similar_to(b2, wavelength_tolerance=tol,
                                  direction_tolerance=tol,
                                  polarization_normal_tolerance=tol,
                                  polarization_fraction_tolerance=tol)
      s0_1 = matrix.col(b1.get_unit_s0())
      s0_2 = matrix.col(b2.get_unit_s0())
      assert s0_1.accute_angle(s0_2, deg=True) < 0.0057 # ~0.1 mrad
    for c1, c2 in zip(reg_exp.crystals(), ref_exp.crystals()):
      assert c1.is_similar_to(c2)

    for d1, d2 in zip(reg_exp.detectors(), ref_exp.detectors()):
      assert d1.is_similar_to(d2,
        fast_axis_tolerance=1e-4, slow_axis_tolerance=1e-4, origin_tolerance=1e-2)

    os.chdir(cwd)
    print ("OK")

  def run_all(self):
    self.test_refine_metrology(300)
    self.test_refine_metrology(300, "hierarchical")
    self.test_refine_metrology(3000)

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  tester = test_cspad_cbf_metrology()
  if len(sys.argv) == 2:
    dataset = int(sys.argv[1])
    tester.test_refine_metrology(dataset)
  else:
    tester.run_all()
