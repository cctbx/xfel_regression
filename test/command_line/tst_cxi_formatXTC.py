from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os
from dials.command_line.dials_import import phil_scope

class test_formatXTC_cxi(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def get_short_timestamp_from_cbfname(self, x):
    return x[5:23]

  def test_cxid1916(self):
    try:
      import psana # testing if psana is present
    except ImportError as e:
      print ("Skipping CXID9114 regression test: PSANA could not be imported")
      return

    if self.xfel_regression is None:
      print ("Skipping CXID9114 regression test: xfel_regression not present")
      return

    cxi_dir = os.path.join(self.xfel_regression, "image_examples/LCLS_XTC_data")
    if not os.path.exists(cxi_dir):
      print ("Skipping CXID9114 regression test: LCLS_XTC_data/test/cxi_test not present")
      return

# Set necessary environmental variables for PSANA to work
    os.environ['SIT_DATA']=os.path.join(cxi_dir,"regg/psdm/data")
    os.environ['SIT_PSDM_DATA']=cxi_dir

# Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_cxi0")
    os.chdir(tmp_dir)

    print ("Assertion results will be found at", tmp_dir)

# First create cbf files using cctbx.xfel.xtc_process
    xtc_process_input = 'input.experiment=cxid9114 input.run_num=108 input.address=CxiDs2.0:Cspad.0\
                         dispatch.find_spots=False dispatch.dump_all=True format.cbf.detz_offset=100.1204\
                         dispatch.datasource=exp=cxid9114:run=108:xtc'

    command = os.path.join("cctbx.xfel.xtc_process %s"%xtc_process_input)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()
# define the custom phil scope to change the detector parameters
    from libtbx.phil import parse
    my_phil_scope = parse('''
      geometry {
        detector {
          panel {
            origin=0.014,0.0,-100.0
          }
        }
      }
''')
    custom_phil_scope = phil_scope.fetch(my_phil_scope)
    custom_phil_params = custom_phil_scope.extract()

# Set paths for the xtc file
    params_path = os.path.join(cxi_dir, 'test/cxi_test/cxi_params.phil')
    data_path = os.path.join(cxi_dir, 'test/cxi_test/cxi_data.loc')
# Look up all the cbf files generated and sort their names according to the timestamps
    import glob
    flist = glob.glob('*.cbf')
    flist_sorted = sorted(flist,key=self.get_short_timestamp_from_cbfname)
# Now read in these individual datablock files and test the geometry, beam and data between cbf and xtc
    from dxtbx.model.experiment_list import ExperimentListFactory

    experiments_xtc = ExperimentListFactory.from_filenames([data_path], load_models=False)
    experiments_cbf = ExperimentListFactory.from_filenames(flist_sorted)
    imageset_xtc = experiments_xtc.imagesets()[0]
    imageset_cbf = experiments_cbf.imagesets()[0]
    from dials.command_line.dials_import import ManualGeometryUpdater
#    manual_geom = ManualGeometryUpdater(custom_phil_params)
#    manual_geom(imageset_xtc)
    assert len(imageset_xtc) == len(imageset_cbf), 'Length of imagesets not equal'
    for i in range(len(imageset_xtc)):
      imageset_cbf_individual_file = ExperimentListFactory.from_filenames([flist_sorted[i]]).imagesets()[0]
      assert str(imageset_xtc.get_detector(i)) == str(imageset_cbf_individual_file.get_detector()), 'Mismatch in Detector Model'
      assert str(imageset_xtc.get_beam(i)) == str(imageset_cbf.get_beam(i)), 'Mismatch in Beam Model'
      data_xtc = imageset_xtc.get_raw_data(0)
      data_cbf = imageset_cbf.get_raw_data(0)
      for panel_num in range(len(imageset_xtc.get_raw_data(0))):
        bool_arr = data_xtc[panel_num] == data_cbf[panel_num]
        assert bool_arr.count(False) == 0,'Mismatch in the data in panel_num=%s'%panel_num
    print ("OK")

  def run_all(self):
      import psana # testing if psana is present
      self.test_cxid1916()

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  message = ''' Test for FormatXTCCspad in dxtbx. Compares the image object interpreted by FormatXTCCspad with the CBF output from xtc_process'''
  print (message)
  show_times_at_exit()
  tester = test_formatXTC_cxi()
  tester.run_all()
