from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os

class test_cspad_quadrants(object):
  def __init__(self):

    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def test_thermolysin_max(self):
    if self.xfel_regression is None:
      print ("Skipping test_thermolysin_max: xfel_regression not present")
      return

    max_dir = os.path.join(self.xfel_regression, "image_examples", "LCLS_CXI")

    if not os.path.exists(max_dir):
      print ("Skipping test_thermolysin_max: directory with maximum projection %s not present"%max_dir)
      return

    max_path = os.path.join(max_dir, "max-cxi84914-e157-r0021.pickle")

    command = "cspad.quadrants %s distl.detector_format_version='CXI 5.1'"%max_path
    print (command)
    result = easy_run.fully_buffered(command=command, ).raise_if_errors()

    print (result.stdout_lines[-3])

    assert result.stdout_lines[-3] == 'The NEW QUAD translations are: [-4, -4, -1, -7, -13, 1, -7, -6]' or \
           result.stdout_lines[-3] == 'The NEW QUAD translations are: [-4, -4, -2, -6, -13, 1, -7, -6]' # Centos 5, 32 bit

    print ("OK")

  def run_all(self):
    self.test_thermolysin_max()


if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  tester = test_cspad_quadrants()
  tester.run_all()

