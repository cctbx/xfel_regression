from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os
from dials.command_line.dials_import import phil_scope

class test_formatXTC_mfx(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def get_short_timestamp_from_cbfname(self, x):
    return x[5:23]

  def test_mfxo1916(self):
    if self.xfel_regression is None:
      print ("Skipping MFXO1916 regression test: xfel_regression not present")
      return

    mfx_dir = os.path.join(self.xfel_regression, "image_examples/LCLS_XTC_data")
    if not os.path.exists(mfx_dir):
      print ("Skipping MFXO1916 regression test: LCLS_XTC_data/test/mfx_test not present")
      return

# Set necessary environmental variables for PSANA to work
    os.environ['SIT_DATA']=os.path.join(mfx_dir,"regg/psdm/data")
    os.environ['SIT_PSDM_DATA']=mfx_dir

# Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_mfx0")
    os.chdir(tmp_dir)

    print ("Assertion results will be found at", tmp_dir)

# First create cbf files using cctbx.xfel.xtc_process
    xtc_process_input = 'input.experiment=mfxo1916 input.run_num=20 input.address=MfxEndstation.0:Rayonix.0\
                         dispatch.find_spots=False dispatch.dump_all=True format.cbf.detz_offset=59.65\
                         format.cbf.mode=rayonix format.cbf.rayonix.override_beam_x=968\
                         format.cbf.rayonix.override_beam_y=965 dispatch.datasource=exp=mfxo1916:run=20:xtc'
    command = os.path.join("cctbx.xfel.xtc_process %s"%xtc_process_input)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()

# define the custom phil scope to change the detector parameters
    from libtbx.phil import parse
    my_phil_scope = parse('''
      geometry {
        detector {
          panel {
            origin=-85.7083,85.4427,-59.65
          }
        }
      }
''')
    custom_phil_scope = phil_scope.fetch(my_phil_scope)
    custom_phil_params = custom_phil_scope.extract()

# Set paths for the xtc file
    params_path = os.path.join(mfx_dir, 'test/mfx_test/mfx_params.phil')
    data_path = os.path.join(mfx_dir, 'test/mfx_test/mfx_data.loc')
# Look up all the cbf files generated and sort their names according to the timestamps
    import glob
    flist = glob.glob('*.cbf')
    flist_sorted = sorted(flist,key=self.get_short_timestamp_from_cbfname)

# Now read in these individual datablock files and test the geometry, beam and data between cbf and xtc
    from dxtbx.model.experiment_list import ExperimentListFactory

    experiments_xtc = ExperimentListFactory.from_filenames([data_path], load_models=False)
    experiments_cbf = ExperimentListFactory.from_filenames(flist_sorted)
    imageset_xtc = experiments_xtc.imagesets()[0]
    imageset_cbf = experiments_cbf.imagesets()[0]
    from dials.command_line.dials_import import ManualGeometryUpdater
    manual_geom = ManualGeometryUpdater(custom_phil_params)
    manual_geom(imageset_xtc)
    assert len(imageset_xtc) == len(imageset_cbf), 'Length of imagesets not equal'
    for i in range(len(imageset_xtc)):
      imageset_cbf_individual_file = ExperimentListFactory.from_filenames([flist_sorted[i]]).imagesets()[0]
      assert str(imageset_xtc.get_detector(i)) == str(imageset_cbf_individual_file.get_detector()), 'Mismatch in Detector Model'
      assert str(imageset_xtc.get_beam(i)) == str(imageset_cbf.get_beam(i)), 'Mismatch in Beam Model'
      data_xtc = imageset_xtc.get_raw_data(0)
      data_cbf = imageset_cbf.get_raw_data(0)
      bool_arr = data_xtc[0] == data_cbf[0].as_double()
      assert bool_arr.count(False) == 0,'Mismatch in the data'
    print ("OK")

  def run_all(self):
#    try:
    import psana # testing if psana is present
    self.test_mfxo1916()
#    except Exception,e:
#      print 'failed to load PSANA'

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  message = ''' Test for FormatXTCRayonix in dxtbx. Compares the image object interpreted by FormatXTCRayonix with the CBF output from xtc_process'''
  print (message)
  show_times_at_exit()
  tester = test_formatXTC_mfx()
  tester.run_all()
