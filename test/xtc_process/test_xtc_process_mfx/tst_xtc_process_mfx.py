from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
from scitbx import matrix
import os
from xfel_regression.test.util import mpi_library

message = ''' Test script for testing cctbx.xfel.xtc_process on **Rayonix** data obtained at LCLS
              Note that the script runs xtc_process and compares the following with the provided
              reference results
              1. Beam model
              2. Crystal models
              3. Detector models
              4. Number of reflections in the integrated reflection tables'''


class test_xtc_process_mfx(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def get_short_timestamp_from_cbfname(self, x):
    return x[5:23]

  def test_mfxo1916(self, use_mpi=False):
    if self.xfel_regression is None:
      print ("Skipping MFXO1916 regression test: xfel_regression not present")
      return

    mfx_dir = os.path.join(self.xfel_regression, "image_examples/LCLS_XTC_data")
    if not os.path.exists(mfx_dir):
      print ("Skipping MFXO1916 regression test: LCLS_XTC_data not present")
      return
    input_dir = os.path.join(self.xfel_regression, "test/xtc_process/test_xtc_process_mfx/input")

# Set necessary environmental variables for PSANA to work
    os.environ['SIT_DATA']=os.path.join(mfx_dir,"regg/psdm/data")
    os.environ['SIT_PSDM_DATA']=mfx_dir

# Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_mfx0")
    os.chdir(tmp_dir)


    print ("Assertion results will be found at", tmp_dir)

# First create cbf files using cctbx.xfel.xtc_process
    phil_file = os.path.join(input_dir, 'params.phil')
    mask_file = os.path.join(input_dir, 'pixels.mask')
    xtc_process_input = 'spotfinder.lookup.mask=%s\
                         integration.lookup.mask=%s'%(mask_file, mask_file)
    command = os.path.join("cctbx.xfel.xtc_process %s %s"%(xtc_process_input, phil_file))
    if use_mpi:
      if mpi_library() == "openmpi":
        extra = "--oversubscribe"
      else:
        extra = ""
      command = "mpirun -n 3 %s "%extra + command + " mp.mpi.method=striping"
    else:
      command += ' dispatch.datasource=exp=mfxo1916:run=20:xtc'
    print (command)
    # Can't use raise if errors, because of tracebacks included in failed indexing steps
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True) #.raise_if_errors()
    with open('out.log', 'w') as fout:
      result.show_stdout(out=fout)
    if result.return_code != 0:
      result.show_stdout()
      raise RuntimeError("cctbx.xfel.xtc_process command failed. Error: \n" + "\n".join(result.stderr_lines))

    # Now compare results vs reference results provided
    ref_exp = {}
    curr_exp = {}
    ref_refls = {}
    curr_refls = {}
    deviation = 7 # Difference in number of reflections compared to reference results
                  # Taken from test_stills_process. Will depend on machine arch
    # First load in the reference models. Comparing only integrated expt/refls
    from dxtbx.model.experiment_list import ExperimentListFactory
    from dials.array_family import flex
    # load all the reference experiments
    ref_dir = os.path.join(input_dir, '..', 'reference_results')
    for filename in os.listdir(ref_dir):
      ts = None
      if 'integrated' not in os.path.splitext(filename)[0] or os.path.splitext(filename)[1] != '.expt': continue
      ts = os.path.splitext(filename)[0].split('-')[1].split('_')[0]
      ref_exp[ts] = ExperimentListFactory.from_json_file(filename, check_format=False)
      frefl = os.path.join(ref_dir, filename.split('integrated')[0]+'integrated.refl')
      ref_refls[ts] = flex.reflection_table.from_file(frefl)
    # Now load all the experiments and corresponding refls from the current processing
    tmp_path = os.path.join('.')
    for filename in os.listdir(tmp_path):
      ts = None
      if 'integrated' not in os.path.splitext(filename)[0] or os.path.splitext(filename)[1] != '.expt': continue
      ts = os.path.splitext(filename)[0].split('-')[1].split('_')[0]
      curr_exp[ts] = ExperimentListFactory.from_json_file(filename, check_format=False)
      frefl = os.path.join(ref_dir, filename.split('integrated')[0]+'integrated.refl')
      curr_refls[ts] = flex.reflection_table.from_file(frefl)

    # Assert that we have the same timestamps when comparing with reference
    assert len(curr_exp.keys()) == len(ref_exp.keys()), 'Dissimilar number of timestamps'
    for ts in curr_exp.keys():
      assert ts in ref_exp.keys(), 'Mismatch in timestamps integrated. Please check output'
    # Now compare models for each frame
    # First beam model
    tol = 1.e-5
    for ts in curr_exp.keys():
      b1 = curr_exp[ts].beams()
      b2 = ref_exp[ts].beams()
      assert len(b1) == len(b2) == 1, 'Should have only one beam model'
      assert b1[0].is_similar_to(b2[0], wavelength_tolerance=tol,
                                  direction_tolerance=tol,
                                  polarization_normal_tolerance=tol,
                                  polarization_fraction_tolerance=tol), 'Beam models dissimilar. Please check output directory'
      s0_1 = matrix.col(b1[0].get_unit_s0())
      s0_2 = matrix.col(b2[0].get_unit_s0())
      assert s0_1.accute_angle(s0_2, deg=True) < 0.0057, 's0 vector dissimilar. Please check output directory' # ~0.1 mrad
      # Now test crystal models
      c1 = curr_exp[ts].crystals()
      c2 = ref_exp[ts].crystals()
      assert len(c1) == len(c2) == 1, 'Should have only one crystal model'
      assert c1[0].is_similar_to(c2[0]) ,'Crystal models dissimilar. Please check output directory'
      # Now test detector models
      d1 = curr_exp[ts].detectors()
      d2 = ref_exp[ts].detectors()
      assert len(d1) == len(d2) == 1, 'Should have only one detector model'
      assert d1[0].is_similar_to(d2[0],
        fast_axis_tolerance=1e-4, slow_axis_tolerance=1e-4, origin_tolerance=1e-2), 'Detector models dissimilar. Please check output directory'

      nrefls_curr = len(curr_refls[ts])
      nrefls_ref = len(ref_refls[ts])
      assert nrefls_curr in range(nrefls_ref-deviation, nrefls_ref+deviation), 'Mismatch in integrated reflections too high. Please check output files'

    print ("OK")

  def run_all(self):
    self.test_mfxo1916(use_mpi=False)
    self.test_mfxo1916(use_mpi=True)

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  print (message)
  show_times_at_exit()
  tester = test_xtc_process_mfx()
  tester.run_all()
