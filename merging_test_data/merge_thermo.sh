#!/usr/bin/env bash

# data from cxi49812, aka workshop experiment cxi84914, e157.
# At LCLS under /reg/d/psdm/cxi84914/scratch/nksauter/results/e157/r0021/003/integration/int-s00*
# Only first 1000 images of stream 00 are included
#

# Seems to be needed for the psana-conda system using MKL
export MKL_THREADING_LAYER=GNU

datastring="data=`libtbx.find_in_repositories xfel_regression/merging_test_data/integration_pickles`"

if [[ `python -c 'import sys; print(sys.version_info[0])'` == '3' ]];then
  datastring=${datastring}_py3
fi

tag="2tli"

postrefine=$1
error_model=$2
postrefine_algorithm=$3
include_negatives=$4
seed=$5

effective_params="d_min=2.1 \
output.n_bins=10 \
${datastring} \
model=`libtbx.find_in_repositories xfel_regression/merging_test_data/${tag}.pdb` \
backend=FS \
pixel_size=0.11 \
nproc=1 \
merge_anomalous=False \
plot_single_index_histograms=False \
raw_data.${error_model}=True \
scaling.mtz_file=`libtbx.find_in_repositories xfel_regression/merging_test_data/${tag}.mtz` \
scaling.mtz_column_F=iobs \
scaling.show_plots=False \
scaling.algorithm=mark0 \
scaling.log_cutoff=2. \
scaling.show_plots=False \
scaling.report_ML=True \
postrefinement.enable=${postrefine} \
postrefinement.algorithm=${postrefine_algorithm} \
include_negatives=${include_negatives} \
set_average_unit_cell=True \
rescale_with_average_cell=False \
significance_filter.sigma=0.5 \
min_corr=0.1 \
output.prefix=thermoanom_${tag} \
raw_data.error_models.sdfac_refine.random_seed=${seed}"

eff_params2=`echo $effective_params|sed -e "s/anomalous=False/anomalous=True/g"|sed -e "s/anom_/noanom_/g"`

cxi.merge ${effective_params}
cxi.xmerge ${effective_params}

cxi.merge ${eff_params2}
cxi.xmerge ${eff_params2}
