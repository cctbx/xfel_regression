from __future__ import division
from libtbx import easy_pickle
from cctbx.crystal_orientation import crystal_orientation
import sys, os

"""
Script to patch up the prime test data, which had bytes objects for dictionary keys
"""

data = easy_pickle.load(sys.argv[1])
new_data = {}

for key in data.keys():
  value = data[key]
  if type(value) == list:
    assert len(value) == 1
  if key == b'observations':
    o = value[0]
    for subkey in list(o.__dict__.keys()):
        subvalue = o.__dict__.pop(subkey)
        if subkey == b'_info':
            o.__dict__[subkey.decode()] = None
        else:
            o.__dict__[subkey.decode()] = subvalue
    value = [o]
  elif key == b'current_orientation':
    value = [crystal_orientation(value[0])]
  new_data[key.decode()] = value

print(data)
print(new_data)
easy_pickle.dump(os.path.basename(sys.argv[1]), new_data)
