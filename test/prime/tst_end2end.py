from __future__ import division, print_function
import libtbx.load_env
import prime.command_line.postrefine as primeEngine
import unittest, os

class KnownValues(unittest.TestCase):

  knownCC12Values = {'sfc_I': 0.9374}
  xfel_regression = libtbx.env.find_in_repositories(
      relative_path="xfel_regression", test=os.path.isdir)

  def testPrimeEnd2End(self):
    """from a list of input.phil files, run prime and compare log result with known values"""
    #setup working dir
    dataDir = os.path.join(self.xfel_regression, "prime_test_data")
    testPaths = ['sfc_I']
    #setup output dir
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_prime_end2end")
    os.chdir(tmp_dir)
    for path in testPaths:
      dataDirTest = os.path.join(dataDir, path)
      #run prime
      mdh = primeEngine.run([os.path.join(dataDirTest, 'prime.phil'), \
          'data=%s'%(os.path.join(dataDirTest, 'integration'))])
      print ('result=%6.4f knownValue=%6.4f'%(mdh.get_cc12()[0], self.knownCC12Values[path]))
      self.assertTrue((mdh.get_cc12()[0] - self.knownCC12Values[path]) > -0.015)

if __name__ == "__main__":
  unittest.main()
