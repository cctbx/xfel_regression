#json input file list
# See https://github.com/mewall/lunus/blob/master/test/unit_tests/TestLunusDir/lunus_params_unique.sh
jsonlist_name=jsonlist.inp
# integration
xvectors_path=${work_dir}/raw/tmpdir_common/xvectors.bin
diffuse_lattice_dir=${work_dir}/lattices
diffuse_lattice_prefix=TestLunus
lunus_image_dir=${work_dir}/proc
resolution=1.6
points_per_hkl=2
overall_scale_factor=1.
filterhkl=True
writevtk=True
#image mask
thrshim_max=10
thrshim_min=0
punchim_xmax=1560
punchim_xmin=0
punchim_ymax=1210
punchim_ymin=1155
windim_xmax=2443
windim_xmin=20
windim_ymax=2507
windim_ymin=20
#mode filter
modeim_bin_size=1
modeim_kernel_width=3
#modeim_kernel_width=15
#polarization correction
distance_mm=57.42
polarim_offset=0.0
polarim_polarization=1.0
#image scale factor parameters, for merge
scale_inner_radius=200
scale_outer_radius=800
#use panel metrology info in .json file
use_json_metrology=True
#correct_offset=True
