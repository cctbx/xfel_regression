from __future__ import division
import os
import subprocess
import pytest
import libtbx.load_env
import shutil
import numpy as np
from xfel_regression.test.util import mpi_library

def test_lunus_worker(dials_data, run_in_tmpdir):
  from dxtbx.model.experiment_list import ExperimentList
  from dxtbx.imageset import ImageSetFactory
  pytest.importorskip("mpi4py")

  xfel_regression_dir = libtbx.env.find_in_repositories(relative_path="xfel_regression", test=os.path.isdir)
  lunus_dir = os.path.join(xfel_regression_dir, 'lunus')

  grid_path = dials_data("thaumatin_grid_scan", pathlib=True)

  experiments = ExperimentList.from_file(os.path.join(lunus_dir, 'combined.expt'), check_format=False)
  assert len(experiments) == 3

  for experiment in experiments:
    img_name = os.path.basename(experiment.imageset.paths()[0])
    img_path = os.path.join(grid_path, img_name)
    experiment.imageset = ImageSetFactory.make_imageset([img_path])

  experiments.as_file('combined.expt')
  shutil.copy(os.path.join(lunus_dir, 'combined.refl'), '.')

  params_file = "lunus_merge.phil"
  shutil.copy(os.path.join(lunus_dir, params_file), '.')

  deck_file = "deck.sh"
  shutil.copy(os.path.join(lunus_dir, deck_file), '.')

  command = "mpirun -n 3 "
  if mpi_library() == 'openmpi':
    command += '--oversubscribe '
  command += "--map-by ppr:3:node:PE=2 cctbx.xfel.merge lunus_merge.phil"

  env = os.environ
  env['OMP_NUM_THREADS'] ='2'

  proc = subprocess.Popen(command.split(),
      stdout=subprocess.PIPE, stderr=subprocess.PIPE,
      env = env
      )
  out, err = proc.communicate()
  assert not err

  control = np.loadtxt(os.path.join(lunus_dir, 'results.hkl'))[:,3]
  query = np.loadtxt('results.hkl')[:,3]
  cc = np.corrcoef(control, query)[0,1]
  assert cc >= 0.99999, cc
