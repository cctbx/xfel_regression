from __future__ import absolute_import, division, print_function
from xfel.merging.application.modify.cosym_tranch_align import alignment_by_embedding,alignment_by_sequential_trial

def counts_for_unit_test(report):
  """Once the tranches are aligned
         tranche0 tranche1 tranche2
   coset0    A       C        E
   coset1    B       D        F
   The total number of datasets is union (A,....,F)
   This should also be equal to union (A,C,E) + union (B,D,F)
   Not true prior to alignment.
  """
  n_tranches = len(report)
  n_cosets = len(report[0])
  coset_totals = [set() for c in range(n_cosets)]
  total = set()
  for itranche in range(n_tranches):
    for icoset in range(n_cosets):
      cut = set(report[itranche][icoset])
      total = total.union(cut)
      coset_totals[icoset] = coset_totals[icoset].union(cut)
  test = 0
  for icoset in range(n_cosets):
    print("coset",icoset,"total",len(coset_totals[icoset]))
    test += len(coset_totals[icoset])
  print("Aligned total is",test,"grand total is",len(total), "delta",test-len(total))
  return test-len(total)

if __name__=="__main__":
  """unit tests drawing on data from xfel_regression.
  Test 1.  Alignment by embedding for single merohedral twin law (n_dim==2)
           xfel_regression/merging_test_data/cosym_input_data/5sya.pickle
  """
  import pickle,os,libtbx.load_env
  xf = libtbx.env.find_in_repositories("xfel_regression")
  with open(os.path.join(xf,"merging_test_data","cosym_input_data","5sya.pickle"),"rb") as F:
    reports = pickle.load(F)
  align = alignment_by_embedding(reports,plot=False)
  assert counts_for_unit_test(reports) > 0
  assert counts_for_unit_test(align) == 0
  print ("OK")

  """Test 1b.  Same data, but alignment by sequential trial"""
  align = alignment_by_sequential_trial(reports,plot=False)
  assert counts_for_unit_test(reports) > 0
  assert counts_for_unit_test(align) == 0
  print ("OK")

  """Test 2.  Alignment by sequential trial for P3_2 case 0f merohedral twinning (n_dim==4)
           xfel_regression/merging_test_data/cosym_input_data/4qfl.pickle"""
  with open(os.path.join(xf,"merging_test_data","cosym_input_data","4qfl.pickle"),"rb") as F:
    reports = pickle.load(F)
  align = alignment_by_sequential_trial(reports,plot=False)
  assert counts_for_unit_test(reports) > 0
  assert counts_for_unit_test(align) < 20
  print ("OK")
