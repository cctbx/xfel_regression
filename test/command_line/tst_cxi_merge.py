from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os

class test_cxi_merge(object):
  def __init__(self):

    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)

  def test_merge_themolysin(self, postrefine = False, error_model = None, cc_onehalfs = None, postrefine_algorithm = "rs", include_negatives = False):
    if self.xfel_regression is None:
      print ("Skipping test_merge_themolysin: xfel_regression not present")
      return

    merging_dir = os.path.join(self.xfel_regression, "merging_test_data")

    if not os.path.exists(merging_dir):
      print ("Skipping test_merge_themolysin: merging directory %s not present"%merging_dir)
      return

    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="test_cxi_merge")
    os.chdir(tmp_dir)

    print ("Merging results will be found at", tmp_dir)

    if error_model == "sdfac_refine":
      seed = "42"
    else:
      seed = "None"

    command = os.path.join(merging_dir, "merge_thermo.sh %s %s %s %s %s"%
      (str(postrefine), error_model, postrefine_algorithm, str(include_negatives), seed))
    print (command)
    result = easy_run.fully_buffered(command=command).raise_if_errors()

    noanom_log = "thermonoanom_2tli_mark0.log"
    assert os.path.exists(noanom_log)

    if include_negatives:
      offset = -7
    else:
      offset = -8

    f = open(noanom_log)
    result = f.readlines()[offset]
    cc = float(result.split()[2].strip("%"))
    if postrefine_algorithm == 'eta_deff':
      from libtbx.test_utils import approx_equal
      assert approx_equal(cc, cc_onehalfs[0], eps=1)
    else:
      assert cc == cc_onehalfs[0], (cc, cc_onehalfs[0])

    anom_log = "thermoanom_2tli_mark0.log"
    assert os.path.exists(anom_log)

    f = open(anom_log)
    result = f.readlines()[offset]
    cc = float(result.split()[2].strip("%"))
    if postrefine_algorithm == 'eta_deff':
      from libtbx.test_utils import approx_equal
      assert approx_equal(cc, cc_onehalfs[1], eps=1)
    else:
      assert cc == cc_onehalfs[1], (cc, cc_onehalfs[1])

    os.chdir(cwd)

    print ("OK")

  def run_all(self, test_number = None):
    if test_number in [None, 1]:
      self.test_merge_themolysin(False, "sdfac_auto", (42.2, 33.0))
    if test_number in [None, 2]:
      self.test_merge_themolysin(False, "errors_from_sample_residuals", (75.1, 62.4))
    if test_number in [None, 3]:
      self.test_merge_themolysin(True, "sdfac_auto", (56.7, 45.1))
    if test_number in [None, 4]:
      self.test_merge_themolysin(True, "sdfac_auto", (60.8, 46.4), 'rs2')
    if test_number in [None, 5]:
      self.test_merge_themolysin(True, "sdfac_auto", (54.1, 41.6), 'eta_deff')
    if test_number in [None, 6]:
      self.test_merge_themolysin(True, "sdfac_auto", (68.1, 55.5), 'rs_hybrid', True)
    if test_number in [None, 7]:
      self.test_merge_themolysin(False, "sdfac_refine", (56.5, 46.3))

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()
  import sys
  if len(sys.argv) > 1:
    test_number = int(sys.argv[1])
  else:
    test_number = None

  tester = test_cxi_merge()
  tester.run_all(test_number)

