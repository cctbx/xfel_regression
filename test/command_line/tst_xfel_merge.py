from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os
import sys
from libtbx.test_utils import approx_equal
from xfel_regression.test.util import mpi_library

# If create_ref == True, the output data will be copied to the reference data
# If create_ref == False, the output data will be matched against the reference data
create_ref = False

# To have another test, add another dictionary to the list
# If the test produces a merged dataset, mtz file, set "merged_data_out":True
all_tests = [
            {"id":"all_1", "merged_data_out":True,   "workers":"all", "error_model":"errors_from_sample_residuals", "set_average_unit_cell":True},
            {"id":"all_2", "merged_data_out":True,   "workers":"all", "postrefine":True, "postrefine_algorithm":"rs", "error_model":"errors_from_sample_residuals", "set_average_unit_cell":True},
            {"id":"input",                           "workers":"input"},
            {"id":"model_scaling",                   "workers":"input,model_scaling"},
            {"id":"modify",                          "workers":"input,model_scaling,modify"},
            {"id":"filter",                          "workers":"input,model_scaling,filter"},
            {"id":"modify_reindex_to_reference",     "workers":"input,model_scaling,modify,filter,modify_reindex_to_reference"},
            {"id":"modify_cosym",                    "workers":"input,balance,model_scaling,modify,filter,modify_cosym"},
            {"id":"modify_cosym_p1",                 "workers":"input,balance,model_scaling,modify,filter,modify_cosym"},
            {"id":"errors_premerge",                 "workers":"input,model_scaling,errors_premerge", "error_model":"ha14"},
            {"id":"scale",                           "workers":"input,model_scaling,scale", "postrefine":False, "tol_cc": .999999},
            {"id":"mark1",                           "workers":"input,model_scaling,scale,postrefine"},
            {"id":"postrefine",                      "workers":"input,model_scaling,postrefine", "postrefine":True, "postrefine_algorithm":"rs"},
            {"id":"statistics_unitcell",             "workers":"input,model_scaling,statistics_unitcell"},
            {"id":"statistics_beam",                 "workers":"input,statistics_beam"},
            {"id":"model_statistics",                "workers":"input,model_scaling,statistics_unitcell,model_statistics"," set_average_unit_cell":True},
            {"id":"statistics_resolution",           "workers":"input,model_scaling,statistics_resolution"},
            {"id":"group",                           "workers":"input,model_scaling,group"},
            {"id":"errors_merge",                    "workers":"input,model_scaling,group,errors_merge", "error_model":"errors_from_sample_residuals"},
            {"id":"errors_merge_ev11",               "workers":"input,model_scaling,group,errors_merge", "error_model":"ev11"},
            {"id":"errors_merge_mm24",               "workers":"input,model_scaling,scale,group,errors_merge", "error_model":"mm24"},
            {"id":"statistics_intensity",            "workers":"input,model_scaling,filter,group,statistics_intensity"},
            {"id":"statistics_deltaccint",           "workers":"input,model_scaling,group,statistics_deltaccint"},
            {"id":"merge", "merged_data_out":True,   "workers":"input,statistics_beam,model_scaling,group,merge"},
            {"id":"statistics_intensity_cxi", "merged_data_out":True, "workers":"input,statistics_beam,model_scaling,group,merge,statistics_intensity_cxi"},
            {"id":"filter_global",                   "workers":"input,filter_global"}
            ]

class test_xfel_merge(object):
  def __init__(self):
    # Set the parent input data directory
    self.merging_input_dir = None
    self.xfel_regression_dir = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)
    if self.xfel_regression_dir != None:
      self.merging_input_dir = os.path.join(self.xfel_regression_dir, "merging_test_data")

    # Set the merging app directory
    self.cctbx_project_dir = libtbx.env.find_in_repositories(
        relative_path="cctbx_project",
        test=os.path.isdir)
    self.merging_app_dir = None
    if self.cctbx_project_dir != None:
      self.merging_app_dir = os.path.join(self.cctbx_project_dir, "xfel", "merging", "application")

  def check_directories_exist(self):
    for test_dir in [self.merging_input_dir, self.merging_app_dir]:
      if not os.path.exists(test_dir):
        print ("Skipping tst_xfel_merge: directory %s not present"%test_dir)
        return False
    return True

  def run_test(self, test_id=None):
    if not self.check_directories_exist():
      return

    if test_id == None:
      test_to_run = all_tests
    else:
      for n,test in enumerate(all_tests):
        if test["id"] == test_id:
          test_to_run = [all_tests[n]]

    for test in test_to_run:
      executor = test_executor(test, self.merging_input_dir)
      executor.run()

class test_executor(object):
  def __init__(self, params, merging_input_dir):
    self.params = params
    self.merging_input_dir = merging_input_dir

    # Set specific sub-directories for input and reference data
    self.input_data_dir = None
    self.reference_data_dir = None
    if self.merging_input_dir != None:
      if self.params["workers"] == "all" or "deltaccint" in self.params["workers"]:
        self.input_data_dir = os.path.join(self.merging_input_dir, "big_input_data")
      elif "reindex_to_reference" in self.params["workers"]:
        self.input_data_dir = os.path.join(self.merging_input_dir, "psI_input_data")
      elif "cosym" in self.params["workers"]:
        self.input_data_dir = os.path.join(self.merging_input_dir, "cosym_input_data")
      else:
        self.input_data_dir = os.path.join(self.merging_input_dir, "small_input_data")
      self.reference_data_dir = os.path.join(self.merging_input_dir, "reference_data")

    # Memorize the current working directory
    self.cwd = os.path.abspath(os.curdir)

    if 'tol_cc' in self.params: self.tol_cc = self.params['tol_cc']
    else: self.tol_cc = 0.99

  def check_directories_exist(self):
    for test_dir in [self.input_data_dir, self.reference_data_dir]:
      if not os.path.exists(test_dir):
        print ("Skipping tst_xfel_merge: directory %s not present"%test_dir)
        return False
    return True

  def run(self):
    if not self.check_directories_exist():
      return

    # Create a temporary directory and switch to it
    from libtbx.test_utils import open_tmp_directory
    tmp_dir = open_tmp_directory(suffix="_%s"%self.params["id"])
    os.chdir(tmp_dir)

    # Create an ouput directory and memorize it
    self.output_dir = os.path.abspath(os.path.join(os.curdir, "out"))
    os.mkdir(self.output_dir)

    # Set up various parameters to be passed to the merging shell script
    if self.params['id'] == "filter":
      filter_d_min = 1.9
    else:
      filter_d_min = None

    # Models
    scaling_algorithm = "mark0"
    scaling_unitcell = "None"
    scaling_spacegroup = "None"
    if "reindex_to_reference" in self.params["workers"]:
      scaling_model_file_path = os.path.join(self.input_data_dir, "PSI_LCLS.mtz")
      statistics_model_file_path = os.path.join(self.input_data_dir, "PSI_LCLS.mtz")
      mtz_column_F = "I-obs"
      filter_unit_cell = "None"
    elif "cosym" in self.params["workers"]:
      scaling_model_file_path = os.path.join(self.input_data_dir, "PSI_refine_429.pdb")
      statistics_model_file_path = os.path.join(self.input_data_dir, "PSI_LCLS.mtz")
      mtz_column_F = "I-obs"
      filter_unit_cell = "None"
    elif self.params['id'] == "mark1":
      scaling_algorithm = "mark1"
      scaling_model_file_path = "None"
      statistics_model_file_path = "None"
      mtz_column_F = "None"
      filter_unit_cell = scaling_unitcell = "79.0,79.0,38.3,90,90,90"
      scaling_spacegroup = "P43212"
    else:
      scaling_model_file_path = os.path.join(self.input_data_dir, "4ngz.pdb")
      statistics_model_file_path = os.path.join(self.input_data_dir, "4ngz.mtz")
      mtz_column_F = "f(+)"
      filter_unit_cell = "79.0,79.0,38.3,90,90,90"

    # Cores
    #number_of_cores = 60 if self.params["workers"] == "all" else 1
    if "cosym" in self.params["workers"]: number_of_cores=5
    else: number_of_cores = 1

    # Cosym SG
    if self.params["id"]=="modify_cosym":
      cosym_sg = "P6"
    elif self.params["id"]=="modify_cosym_p1":
      cosym_sg = "P1"
    else:
      cosym_sg = "P1"

    if self.params["id"]=="modify_cosym" or self.params["id"]=="modify_cosym_p1":
      override_identifiers=False
    else:
      override_identifiers=True

    # Input file extensions
    if self.params["workers"] == "all" or "deltaccint" in self.params["workers"]:
      expt_suffix = "_integrated_experiments.json"
      refl_suffix = "_integrated.pickle"
    elif (
        "reindex_to_reference" in self.params["workers"] or
        "cosym" in self.params["workers"]
        ):
      expt_suffix = ".expt"
      refl_suffix = ".refl"
    else:
      expt_suffix = ".json"
      refl_suffix = ".pickle"

    # Define the output and reference paths and extensions
    output_prefix = "test_%s"%self.params["id"]

    # If the test results are platform-specific, we use platform-specific references.
    # Postrefinement results are known to be platform-specific.
    platform_tag = self.get_platform_tag() if ("postrefine" in self.params and self.params["postrefine"] == True) else ''

    # If the test produces a merged dataset, we compare that dataset with the reference.
    # Else we have the last worker in the chain output its experiments and reflections, and then we compare those data with the reference.
    # We also optionally compare some log file metrics with the reference log file metrics
    self.test_produces_merged_data = "merged_data_out" in self.params and self.params["merged_data_out"] == True
    if self.test_produces_merged_data:
      save_experiments_and_reflections = False
      self.output_merged_data_file_path    = os.path.join(self.output_dir, output_prefix + "_all.mtz")
      self.log_file_path = os.path.join(self.output_dir, output_prefix + "_main.log")
      self.reference_merged_data_file_path = os.path.join(self.reference_data_dir, output_prefix + "_all_reference%s.mtz"%platform_tag)
      self.reference_log_file_path = os.path.join(self.reference_data_dir, output_prefix + "_main_reference%s.log"%platform_tag)
    else:
      save_experiments_and_reflections = True
      self.output_expt_file_path = os.path.join(self.output_dir, output_prefix + ".expt")
      self.output_refl_file_path = os.path.join(self.output_dir, output_prefix + ".refl")
      if self.params['id'] == 'mark1':
        self.reference_expt_file_path = os.path.join(self.reference_data_dir, "test_model_scaling_reference%s.expt"%platform_tag)
        self.reference_refl_file_path = os.path.join(self.reference_data_dir, "test_model_scaling_reference%s.refl"%platform_tag)
      else:
        self.reference_expt_file_path = os.path.join(self.reference_data_dir, output_prefix + "_reference%s.expt"%platform_tag)
        self.reference_refl_file_path = os.path.join(self.reference_data_dir, output_prefix + "_reference%s.refl"%platform_tag)

    # Set other parameters for the merging shell script
    postrefine            = self.params["postrefine"]           if "postrefine" in self.params            else False
    postrefine_algorithm  = self.params["postrefine_algorithm"] if "postrefine_algorithm" in self.params  else "rs"
    error_model           = self.params["error_model"]          if "error_model" in self.params           else "errors_from_sample_residuals"
    merge_anomalous       = self.params["merge_anomalous"]      if "merge_anomalous" in self.params        else False
    set_average_unit_cell = self.params["set_average_unit_cell"] if "set_average_unit_cell" in self.params else False

    try:
      if mpi_library() == "openmpi":
        mpi_extra = "--oversubscribe"
      else:
        mpi_extra = "''"
    except RuntimeError:
      mpi_extra = "''"

    # Execute the merging shell script
    command = os.path.join(self.merging_input_dir, "xfel_merge.sh %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s '%s' %s %s %s %s '%s' %s %s %s"%(
                                                                 self.params["workers"],
                                                                 self.input_data_dir,
                                                                 expt_suffix,
                                                                 refl_suffix,
                                                                 scaling_model_file_path,
                                                                 statistics_model_file_path,
                                                                 str(postrefine),
                                                                 postrefine_algorithm,
                                                                 error_model,
                                                                 str(merge_anomalous),
                                                                 str(set_average_unit_cell),
                                                                 str(save_experiments_and_reflections),
                                                                 self.output_dir,
                                                                 output_prefix,
                                                                 str(number_of_cores),
                                                                 mtz_column_F,
                                                                 filter_unit_cell,
                                                                 cosym_sg,
                                                                 mpi_extra,
                                                                 scaling_algorithm,
                                                                 scaling_unitcell,
                                                                 scaling_spacegroup,
                                                                 str(override_identifiers),
                                                                 str(filter_d_min)))
    result = easy_run.fully_buffered(command=command)
    result.show_stderr()
    result.raise_if_errors()
    #result = easy_run.fully_buffered(command=command).raise_if_output(show_output_threshold=100)

    if create_ref:
      self.copy_results_to_reference()
    else:
      self.compare_results_with_reference()

    # Restore the current working directory
    os.chdir(self.cwd)

    print ("OK")

  def copy_results_to_reference(self):
    print ("Copying results to reference...")
    from shutil import copyfile
    if self.test_produces_merged_data:
      copyfile(self.output_merged_data_file_path, self.reference_merged_data_file_path)
      copyfile(self.log_file_path, self.reference_log_file_path)
    else:
      copyfile(self.output_expt_file_path, self.reference_expt_file_path)
      copyfile(self.output_refl_file_path, self.reference_refl_file_path)

  def compare_results_with_reference(self):
    print ("Comparing results with reference...")
    if self.test_produces_merged_data:
      self.compare_reflection_data(self.output_merged_data_file_path, self.reference_merged_data_file_path)
      self.compare_metrics(self.log_file_path, self.reference_log_file_path)
    elif "cosym" in self.params["workers"]:
      ref_dataframe = os.path.join(self.input_data_dir, "test_reindex_dataframe.pickle")
      out_dataframe = os.path.join(self.output_dir, "test_cosym_dataframe.pickle")
      self.compare_reindex_percent_agreement(
          ref_dataframe, out_dataframe, 0.95, 161, 0.03
      )
      # make sure we wrote out an embedding plot
      out_plotfile = os.path.join(self.output_dir, 'cosym_embedding_1.png')
      assert os.path.exists(out_plotfile)

    elif 'group' not in self.params["workers"]:
      self.compare_experiments(self.output_expt_file_path, self.reference_expt_file_path)
      self.compare_reflection_tables(self.output_refl_file_path, self.reference_refl_file_path)
    elif 'group' in self.params["workers"] and 'merge' not in self.params["workers"].split(','):
      self.compare_reflection_tables(self.output_refl_file_path, self.reference_refl_file_path)

  def compare_metrics(self, file_1, file_2):
    assert os.path.exists(file_1)
    assert os.path.exists(file_2)
    f1 = open(file_1)
    f2 = open(file_2)
    if self.params["id"] in ["all_1", "all_2", "statistics_intensity_cxi"]:
      offset = -7
      result_1 = f1.readlines()[offset]
      result_2 = f2.readlines()[offset]

      cc_iso_1 = float(result_1.split()[4].strip("%"))
      cc_iso_2 = float(result_1.split()[4].strip("%"))
      assert approx_equal(cc_iso_1, cc_iso_2), "CCiso are different"

      print ("Log file metrics check: OK")
    return True

  def compare_experiments(self, file_1, file_2):
    from dxtbx.model.experiment_list import ExperimentListFactory
    expts_1 = ExperimentListFactory.from_json_file(file_1, check_format = False)
    expts_2 = ExperimentListFactory.from_json_file(file_2, check_format = False)
    assert len(expts_1) == len(expts_2), "Experiment files contain different number of experiments (%s vs %s, %d vs. %s)"% \
      (file_1, file_2, len(expts_1), len(expts_2))
    for e1,e2 in zip(expts_1, expts_2):
      assert e1.beam.is_similar_to(e2.beam), "Beam objects are different"
      assert e1.crystal.is_similar_to(e2.crystal), "Crystal objects are different"
      assert e1.detector.is_similar_to(e2.detector), "Detector objects are different"

    print ("Experiments check: OK")
    return

  def compare_reflection_tables(self, file_1, file_2):

    def _compare_keys(refls_1, refls_2, file_1, file_2):
      for key in refls_1.keys():
        if key=="flags":
          continue
        else:
          assert key in list(refls_2.keys()), "Test %s: %s vs %s - Reflection tables have different number of columns"%(self.params["id"], file_1, file_2)
      for key in refls_2.keys():
        if key=="flags":
          continue
        else:
          assert key in list(refls_1.keys()), "Test %s: %s vs %s - Reflection tables have different number of columns"%(self.params["id"], file_1, file_2)

    from dials.array_family import flex
    refls_1 = flex.reflection_table.from_file(file_1)
    refls_2 = flex.reflection_table.from_file(file_2)

    # as of cctbx/cctbx_project#822, exp_id is not a valid column in cctbx.xfel.merge
    assert 'exp_id' not in refls_1 # test set
    if 'exp_id' in refls_2:
        del refls_2['exp_id'] # reference set

    assert len(refls_1) == len(refls_2), "Test %s: %s vs %s - Reflection tables have different number of reflections"%(self.params["id"], file_1, file_2)
    _compare_keys(refls_1, refls_2, file_1, file_2)
    # Match every column in the reflection tables. Assume that the input data did have "experiment identifiers",
    # so we didn't have to generate new ones, when we loaded the data. Otherwise the approx_equal test below would fail on "exp_id" columns.
    for key in refls_1.keys():
      if key=="flags": continue
      if type(refls_1[key]) is flex.double:
        print (key, flex.linear_correlation(refls_1[key],refls_2[key]).coefficient())
        assert flex.linear_correlation(refls_1[key],refls_2[key]).coefficient() >= self.tol_cc, \
          "Test %s: %s vs %s - Reflection table columns '%s' are different"%(self.params["id"], file_1, file_2, str(key))
      elif type(refls_1[key]) is flex.int:
        assert flex.linear_correlation(refls_1[key].as_double(),refls_2[key].as_double()).coefficient() >= self.tol_cc, \
          "Test %s: %s vs %s - Reflection table columns '%s' are different"%(self.params["id"], file_1, file_2, str(key))
      else:
        assert approx_equal(refls_1[key],refls_2[key], out=None), \
          "Test %s: %s vs %s - Reflection table columns '%s' are different"%(self.params["id"], file_1, file_2, str(key))
    print ("Reflections check: OK")
    return

  def compare_reindex_percent_agreement(
      self, file_1, file_2, pct_target, n_target, eps_frac=0.01
  ):
    import xfel.merging.application.modify.compare_results as compare
    n_overlap, pct_match = compare.run(file_1, file_2)

    results_str = "{} pct match for {} lattices".format(pct_match, n_overlap)
    assert approx_equal(pct_match, pct_target, eps=eps_frac*pct_target), results_str
    assert approx_equal(n_overlap, n_target, eps=eps_frac*n_target), results_str

  def compare_reflection_data(self, file_1, file_2):
    from iotbx import mtz
    from dials.array_family import flex
    data_1 = mtz.object(file_1)
    data_2 = mtz.object(file_2)

    arrays_1 = data_1.as_miller_arrays()
    arrays_2 = data_2.as_miller_arrays()

    for a_1, a_2 in zip(arrays_1, arrays_2):
      # match types of data
      assert a_1.info().label_string().lower() == a_2.info().label_string().lower(), "Test %s: %s vs %s - Types of data are different"%(self.params["id"], file_1, file_2)
      assert a_1.anomalous_flag() == a_2.anomalous_flag(), "Test %s: %s vs %s - Anomalous flags are different"%(self.params["id"], file_1, file_2)
      # match data
      intensity_array_1 = a_1.as_intensity_array()
      intensity_array_2 = a_2.as_intensity_array()
      matches = intensity_array_1.match_indices(intensity_array_2)
      count_1 = matches.pair_selection(0).count(True)
      count_2 = matches.pair_selection(1).count(True)
      assert abs(1-(count_1/count_2)) <= 0.01, "Test %s: %s vs %s - Arrays have quite different lengths (%d, %d)"% \
        (self.params["id"], file_1, file_2, len(intensity_array_1.indices()), len(intensity_array_2.indices()))
      intensity_array_1 = intensity_array_1.select(matches.pairs().column(0))
      intensity_array_2 = intensity_array_2.select(matches.pairs().column(1))
      cc = flex.linear_correlation(intensity_array_1.data(), intensity_array_2.data()).coefficient()
      assert cc >= self.tol_cc, \
        "Test %s: %s vs %s - Intensities are different (cc: %.1f)"%(self.params["id"], file_1, file_2, 100*cc)
      cc = flex.linear_correlation(intensity_array_1.sigmas(), intensity_array_2.sigmas()).coefficient()
      assert cc >= self.tol_cc, \
        "Test %s: %s vs %s - Sigmas are different (cc: %s.1f)"%(self.params["id"], file_1, file_2, 100*cc)

    print ("Merged data check: OK")
    return

  def get_platform_tag(self):
    '''Create a platform-specific tag. Cases will be added as we discover more platform-specific test results.'''
    tag = ''
    import platform

    # make the build machine display its platform
    #message = str(platform.linux_distribution()) + "|" + str(platform.dist()) + "|" + str(platform.platform()) + "|" + str(platform.python_compiler())
    #assert False, str(message)

    if platform.platform().startswith("Linux"):
      try:
        import distro
      except ImportError:
        sys.stderr.write("For testing, install distro from conda-forge.\n\n")
        raise
      if distro.id() in ("centos", "rhel", "rocky", "ubuntu"):
        if distro.version() == '6':
          tag = "_C6"
        elif distro.version() == '7' or distro.version() == '20.04' \
          or distro.version().startswith('8'):
          tag = "_C7"
      assert tag, "Distro unrecognized: %s"%str(distro.id(), distro.name(), distro.version())
    elif platform.platform().startswith("Darwin") or platform.platform().startswith("macOS"):
      tag = "_Darwin"

    assert tag, "Platform unrecognized: %s"%platform.platform()
    return tag

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  if len(sys.argv) > 1:
    test_id = sys.argv[1]
  else:
    test_id = None

  tester = test_xfel_merge()
  tester.run_test(test_id)
