from __future__ import division, print_function
from xfel.util import dials_pickle_reader
import libtbx.load_env
import shutil, os
from libtbx.test_utils import open_tmp_directory

class test_dials_pickle_reader(object):
  def __init__(self):
    self.xfel_regression_dir = libtbx.env.find_in_repositories(
      relative_path="xfel_regression",
      test=os.path.isdir)
    self.test_integration_pickle = os.path.join(self.xfel_regression_dir, "merging_test_data",
      "integration_pickles", "int-s00-2011-12-02T21:08Z32.984_00000.pickle")
    self.test_experiment_list = os.path.join(self.xfel_regression_dir, "merging_test_data",
      "experiment_lists", "idx-s00-20111202210832984_experiments.json")
    self.test_reflection_table = os.path.join(self.xfel_regression_dir, "merging_test_data",
      "reflection_tables", "idx-s00-20111202210832984_integrated.pickle")

    self.dict_pickle = "int-s00-2011-12-02T21:08Z32.984_00000.pickle"
    self.dials_pickle = "idx-s00-20111202210832984_integrated.pickle"
    self.json = "idx-s00-20111202210832984_experiments.json"

    self.cwd = os.path.abspath(os.curdir)
    self.tmp_dir = open_tmp_directory(suffix="test_dials_pickle_reader")
    os.chdir(self.tmp_dir)
    shutil.copyfile(self.test_integration_pickle, self.dict_pickle)
    shutil.copyfile(self.test_experiment_list, self.json)
    shutil.copyfile(self.test_reflection_table, self.dials_pickle)
    print ("dials_pickle_reader test files located at %s"%(self.tmp_dir))


  def match(self):
    # when no json is supplied, find a compatible json in the same directory
    json = dials_pickle_reader.find_json(self.dials_pickle)
    assert json is not None
    assert os.path.basename(json) == "idx-s00-20111202210832984_experiments.json"

    not_a_json = dials_pickle_reader.find_json("not_a_pickle")
    assert not_a_json is None

    print ("OK")

  def read(self):
    # return a dictionary pickle if readable reflection table and json can be found
    reader = dials_pickle_reader.read_dials_pickle(self.dials_pickle)
    reader.make_pickle()
    assert isinstance(reader.dictionary, dict)

    reader = dials_pickle_reader.read_dials_pickle(self.dials_pickle, self.json)
    reader.make_pickle()
    assert isinstance(reader.dictionary, dict)

    reader = dials_pickle_reader.read_dials_pickle(self.dials_pickle, self.json,
      pickle_ext="_integrated", json_ext="_experiments")
    reader.make_pickle()
    assert isinstance(reader.dictionary, dict)

    reader = dials_pickle_reader.read_dials_pickle(self.dials_pickle, "not_a_json")
    reader.make_pickle()
    assert reader.dictionary is None

    reader = dials_pickle_reader.read_dials_pickle("not_a_pickle", self.json)
    reader.make_pickle()
    assert reader.dictionary is None

    print ("OK")

  def run_all(self):
    self.match()
    self.read()
    os.chdir(self.cwd)

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  tester = test_dials_pickle_reader()
  tester.run_all()
