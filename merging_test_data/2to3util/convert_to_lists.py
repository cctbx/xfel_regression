from __future__ import division, print_function
from cctbx import sgtbx # import dependency
from libtbx import easy_pickle
from cctbx.crystal_orientation import crystal_orientation
import sys, os

"""
Script to read a cxi.merge pickle and write a new pickle with flex arrays converted to lists
Used to convert pickles from py2 to py3
"""

filepath = sys.argv[1]
d = easy_pickle.load(filepath)

new_d = {}
for key in d:
  print (key)
  if key == 'current_orientation':
    new_d['current_orientation'] = [crystal_orientation(d['current_orientation'][0])]
  elif key == 'mapped_predictions':
    a, b = d['mapped_predictions'][0].parts()
    new_d['mapped_predictions'] = list(a), list(b)
  elif key == 'correction_vectors':
    new_d[key] = d[key]
  elif key == 'observations':
    o = d['observations'][0]
    new_d[key] = o.unit_cell(), o.space_group_info(), list(o.indices()), list(o.data()), list(o.sigmas())
  elif isinstance(d[key], list):
    assert len(d[key]) == 1
    if hasattr(d[key][0], '__len__') and not isinstance(d[key][0],str):
      new_d[key] = [list(d[key][0])]
    else:
      new_d[key] = d[key]
  elif hasattr(d[key], '__len__') and not isinstance(d[key][0],str):
    new_d[key] = [list(d[key])]
  else:
    new_d[key] = d[key]

easy_pickle.dump(os.path.basename(filepath), new_d)
